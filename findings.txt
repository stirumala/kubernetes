x509: certificate signed by unknown authority
 
Error Type: 
Failed to pull image "nginx": rpc error: code = Unknown desc = Error response from daemon: Get https://registry-1.docker.io/v2/: x509: certificate signed by unknown authority
  
Source : https://minikube.sigs.k8s.io/docs/handbook/untrusted_certs/
  
Details

You will need a corporate X.509 Root Certificate in PEM format. If it’s in DER format, convert it:
openssl x509 -inform der -in my_company.cer -out my_company.pem

Copy the certificate into the certs directory:

mkdir -p $HOME/.minikube/certs
cp my_company.pem $HOME/.minikube/certs/my_company.pem

Then restart minikube with the --embed-certs flag to sync the certificates:
minikube start --embed-certs
